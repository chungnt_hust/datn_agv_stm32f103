
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2020 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f3xx_hal.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "uart_user.h"
#include "hdlCC1310.h"
#include "irSensor.h"
#include <stdlib.h>
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
#define MAX_SOFT_TIMER 5
volatile uint16_t softTimer[MAX_SOFT_TIMER];
volatile uint32_t g_SysTime;

									/* Chieu quay dong co */
/*===========================================================================*/
#define QUAY_THUAN  0
#define QUAY_NGHICH 1

uint8_t dangChuyenHuong = 0;
uint8_t huong = DIR_DUNG;
uint32_t timeOut;
uint8_t chuyenHuongReq = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
void Led_dimmingProcess(void);
void HAL_SYSTICK_Callback(void)
{
	for(uint8_t i = 0; i < MAX_SOFT_TIMER; i++) softTimer[i]++;
	g_SysTime++;
	Led_dimmingProcess();
}

uint32_t elapsedtime(uint32_t newTime, uint32_t oldTime)
{
  return (newTime - oldTime);
}
																/* PID */
/*===========================================================================*/
#define T_sampling 0.001 //ms
uint8_t kp = 15;
float ki = 0.02;
//	uint8_t init1 =160, init2 = 160;
int16_t init1 = 130, init2 = 183;
uint8_t valuePWM1 = 0, valuePWM2 = 0;
static double error_intergate = 0;
int16_t pid_calculate(uint8_t kp, uint8_t ki, uint8_t kd, int8_t current_error)
{
	
	int16_t u_value = 0;
	error_intergate += ((double)current_error*T_sampling);
	u_value = (int16_t)kp*(int16_t)current_error + (int16_t)(error_intergate*ki);
	
	return u_value;
}
/*===========================================================================*/
void AGV_Dung_lai()
{
	huong = DIR_DUNG;
	valuePWM1 = 0, valuePWM2 = 0;
	__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, 0);
	__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_2, 0);
	chuyenHuongReq = 0;
	dangChuyenHuong = 0;
}

void AGV_Di_Thang()
{
	huong = DIR_DI_THANG;
	HAL_GPIO_WritePin(DIR_CH1_GPIO_Port, DIR_CH1_Pin, (GPIO_PinState)QUAY_THUAN);
	HAL_GPIO_WritePin(DIR_CH2_GPIO_Port, DIR_CH2_Pin, (GPIO_PinState)QUAY_THUAN);
	valuePWM1 = init1, valuePWM2 = init2;
}

void AGV_Re_Trai()
{
	huong = DIR_RE_TRAI;
	chuyenHuongReq = 1;
}

void AGV_Re_Phai()
{
	huong = DIR_RE_PHAI;
	chuyenHuongReq = 1;
}

void AGV_Quay_Dau()
{
	huong = DIR_QUAY_DAU;
	chuyenHuongReq = 1;
}

void (*nextDir)(void);

/* LED dimming */
#define LEVEL_ON  GPIO_PIN_RESET
#define LEVEL_OFF GPIO_PIN_SET
#define MAX_BIT_DIMMING 8
#define LSB_TIME 7 // 7us
uint32_t counterTime[MAX_BIT_DIMMING] = {LSB_TIME - 1, 2*LSB_TIME - 1, 4*LSB_TIME - 1, 8*LSB_TIME - 1, 16*LSB_TIME - 1, 32*LSB_TIME - 1, 64*LSB_TIME - 1, 128*LSB_TIME - 1};
typedef struct
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
} ledRGB_t;
ledRGB_t ledRGB;
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	static volatile uint8_t bitPos = 0;
	uint8_t shift;
	if(htim->Instance == TIM3)
	{
		TIM3->CNT = 0;
		TIM3->ARR = counterTime[bitPos];	
		TIM3->SR = ~TIM_SR_UIF;
		shift = 1<<bitPos;
		((ledRGB.r & shift) != 0) ? HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, LEVEL_ON) : HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, LEVEL_OFF);
		((ledRGB.g & shift) != 0) ? HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, LEVEL_ON) : HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, LEVEL_OFF);
		((ledRGB.b & shift) != 0) ? HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, LEVEL_ON) : HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, LEVEL_OFF);
		if(++bitPos == MAX_BIT_DIMMING) bitPos = 0;
	}
}
#define TIME_DIM_INTERVAL 3
uint32_t timeDim = 0;
uint8_t dimR = 0, upR = 0, downR = 0;
uint8_t dimG = 0, upG = 0, downG = 0;
uint8_t dimB = 0, upB = 0, downB = 0;
void Led_dimmingProcess(void)
{
	if(((dimR == 1) || (dimG == 1) || (dimB == 1)) && elapsedtime(g_SysTime, timeDim) >= TIME_DIM_INTERVAL)
	{
		/* dim R */		
		if(upR == 1){
			ledRGB.r++;
			if(ledRGB.r == 254) { upR = 0; downR = 1; }
		}
		else if(downR == 1){
			ledRGB.r--;
			if(ledRGB.r == 0) { upR = 1; downR = 0; }
		}

		/* dim G */
		if(upG == 1){
			ledRGB.g++;
			if(ledRGB.g == 254) { upG = 0; downG = 1; }
		}
		else if(downG == 1){
			ledRGB.g--;
			if(ledRGB.g == 0) { upG = 1; downG = 0; }
		}

		/* dim B */
		if(upB == 1){
			ledRGB.b++;
			if(ledRGB.b == 254) { upB = 0; downB = 1; }
		}
		else if(downB == 1){
			ledRGB.b--;
			if(ledRGB.b == 0) { upB = 1; downB = 0; }
		}
		timeDim = g_SysTime;
	}
}

#define LED_R 0
#define LED_G 1
#define LED_B 2
#define LED_Y 3
#define ALL_LED 4
void dimLed_En(uint8_t led)
{
	switch(led)
	{
		case LED_R:
			dimR = 1;
			ledRGB.r = 0;
			upR = 1;
			downR = 0;
			break;
		case LED_G:
			dimG = 1;
			ledRGB.g = 0;
			upG = 1;
			downG = 0;
			break;
		case LED_B:
			dimB = 1;
			ledRGB.b = 0;
			upB = 1;
			downB = 0;
			break;
		case LED_Y:
			dimR = dimG = 1;
			ledRGB.r = ledRGB.g = 0;
			upR = upG = 1;
			downR = downG = 0;
			break;
		case ALL_LED:
			dimR = dimG = dimB = 1;
			ledRGB.r = ledRGB.g = ledRGB.b = 0;
			upR = upG = upB = 1;
			downR = downG = downB = 0;
			break;
	}
}

void dimDisable(uint8_t r, uint8_t g, uint8_t b)
{
	dimR = dimG = dimB = 0;
	ledRGB.r = r;
	ledRGB.g = g;
	ledRGB.b = b;
	upR = upG = upB = 0;
	downR = downG = downB = 0;
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART3_UART_Init();
  MX_TIM4_Init();
  MX_USART2_UART_Init();
  MX_USART1_UART_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
  Uart_User_Init();
  AGVSTMpowerOn();
	AGV_Dung_lai();
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_2);
	HAL_TIM_Base_Start_IT(&htim3);
	dimDisable(0, 0, 0);
	dimLed_En(LED_B);

//	HAL_Delay(2000);
	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
    if(softTimer[0] >= 1)
    {
      softTimer[0] = 0;
      Uart_User_Process();
      CC1310_checkHasPayloadEvent();
			
    }
    if(softTimer[1] >= 1)
    {
      softTimer[1] = 0;
			if(huong != DIR_DUNG)
			{
				IR_Scan(); 
				/* Sau khi doc duoc ma vach phai cho toi khi den nga re */
				/* ======================================= */
				if((chuyenHuongReq == 1) && (gSensorIRData == -10))
				{
					printf("XXXX\n");
					chuyenHuongReq = 0;
					switch(huong)
					{
						case DIR_RE_TRAI: valuePWM1 = 0; valuePWM2 = init2; break;
						case DIR_RE_PHAI: valuePWM1 = 199; valuePWM2 = 0; break;
						case DIR_QUAY_DAU:   
						{
							__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, 0);
							__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_2, 0);
							valuePWM1 = 199;
							valuePWM2 = init2;
							HAL_GPIO_WritePin(DIR_CH2_GPIO_Port, DIR_CH2_Pin, (GPIO_PinState)QUAY_NGHICH); 
							HAL_Delay(100);
							break;
						}
					} 
					dangChuyenHuong = 1;				
					timeOut = g_SysTime;
				}
				
				/* Neu khong chuyen huong thi do line */
				/* ======================================= */
				if(dangChuyenHuong == 0)
				{			
					if(gSensorIRData != -10)
					{
						int8_t cur_err = gSensorIRData;
						int16_t u = pid_calculate(kp,ki,0,cur_err);
						
						if((int16_t)init1 + u > 199) {
							valuePWM1 = 199;
						}
						else if((int16_t)init1 + u <= 0) {
							valuePWM1 = 0;		
						}							
						else {
							valuePWM1 = init1 + u;
						}
						if((int16_t)init2 - u > 199) {
							valuePWM2 = 199;
						}
						else if((int16_t)init2 - u <= 130)  {
							valuePWM2 = 0;
						}
						else {
							valuePWM2 = init2 - u;
						}
					}								
				}
				/* Thuc hien chuyen huong */
				/* ======================================= */
				else
				{				
					switch(huong)
					{
						case DIR_RE_TRAI:
							/* Chuyen huong xong */		
							/* ======================================= */
							if(elapsedtime(g_SysTime, timeOut) > 4500)
							{
								if(abs(gSensorIRData) < 4)
								{
									dangChuyenHuong = 0;
									
								}
							} break;
						case DIR_RE_PHAI:
							/* Chuyen huong xong */		
							/* ======================================= */
							if(elapsedtime(g_SysTime, timeOut) > 4300)
							{
								if(abs(gSensorIRData) < 4)
								{
									dangChuyenHuong = 0;
									
								}
							} break;
						/* Chuyen huong xong */		
						/* ======================================= */
						case DIR_QUAY_DAU:
							if(elapsedtime(g_SysTime, timeOut) > 5000)
							{	
								if(abs(gSensorIRData) < 4)
								{
									dangChuyenHuong = 0;
									HAL_GPIO_WritePin(DIR_CH2_GPIO_Port, DIR_CH2_Pin, (GPIO_PinState)QUAY_THUAN);
//									AGV_Dung_lai();
								}
							} break;
						default: break;
					}
					/* ======================================= */
				}
				__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, valuePWM1);
				__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_2, valuePWM2);
			}
    }

		if(softTimer[2] >= 1000)
		{
      softTimer[2] = 0;
      HAL_GPIO_TogglePin(led_GPIO_Port, led_Pin);
      
    }
		
		// for(uint8_t pwm = 0; pwm < 100; pwm++)
		// {
			
		// 	__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_2, pwm);
		// 	HAL_Delay(20);
		// }
  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_USART3;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
