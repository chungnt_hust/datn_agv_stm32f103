/*
 * irSensor.c
 *
 *  Created on: Mar 11, 2020
 *      Author: chungnguyen
 */
#include "irSensor.h"
#include "main.h"

int8_t gSensorIRData = -20;

static GPIO_TypeDef* gpioPort[] = {GPIOA, GPIOA, GPIOA, GPIOA, GPIOB};
static uint16_t gpioPin[] = {GPIO_PIN_4, GPIO_PIN_5, GPIO_PIN_6, GPIO_PIN_7, GPIO_PIN_0};
static struct
{
    uint8_t irOldValue;
    uint8_t irTemValue;
    uint8_t irNewValue;
    uint8_t state;
    uint32_t timeStartActive;
    uint32_t timeTotalActive;
} Control_ir[IR_MAX_PIN];
static uint8_t tempData;

void IR_Scan(void)
{
		for(uint8_t i = 0; i < IR_MAX_PIN; i++)
		{
			Control_ir[i].irTemValue = HAL_GPIO_ReadPin(gpioPort[i], gpioPin[i]);
			Control_ir[i].timeTotalActive = elapsedtime(g_SysTime, Control_ir[i].timeStartActive);
			switch(Control_ir[i].state)
			{
				case IR_STATE_NCHANGE:
						Control_ir[i].irOldValue = Control_ir[i].irTemValue;
						Control_ir[i].timeStartActive = g_SysTime;
						Control_ir[i].state = IR_STATE_DEBOUNCE;
						break;
				case IR_STATE_DEBOUNCE:
						if(Control_ir[i].timeTotalActive > IR_TIME_DEBOUNCE)
						{
								if(Control_ir[i].irTemValue == Control_ir[i].irOldValue)
								{
												Control_ir[i].irNewValue = Control_ir[i].irOldValue;
												// update new value
											//  LOG_INFO("IR value[] = %d%d%d%d%d\n", Control_ir[IR_1].irNewValue, Control_ir[IR_2].irNewValue,
											// 	 												Control_ir[IR_3].irNewValue, Control_ir[IR_4].irNewValue, Control_ir[IR_5].irNewValue);
								}
								Control_ir[i].state = IR_STATE_NCHANGE;
						} break;
				}
		}
		
    tempData = 0;
    for(uint8_t i = 0; i < IR_MAX_PIN; i++)
    {
        tempData <<= 1;
        tempData |= Control_ir[i].irNewValue;        
    }
    switch(tempData)
    {
			#if LINE_LEVEL_1
        case 1: gSensorIRData = 4; break;//0b00000001
        case 3: gSensorIRData = 3; break;//0b00000011
        case 2: gSensorIRData = 2; break;//0b00000010
        case 6: gSensorIRData = 1; break;//0b00000110
        case 4: gSensorIRData = 0; break;//0b00000100
        case 14: gSensorIRData = -1; break;//0b00001100
        case 8:  gSensorIRData = -2; break;//0b00001000
        case 24: gSensorIRData = -3; break;//0b00011000
        case 16: gSensorIRData = -4; break;//0b00010000
			#else
			case 0x1E: gSensorIRData = 4; break;		//0b___11110
			case 0x1C: gSensorIRData = 3; break;		//0b___11100
			case 0x1D: gSensorIRData = 2; break;		//0b___11101
			case 0x19: gSensorIRData = 1; break;		//0b___11001
			case 0x1B: gSensorIRData = 0; break;		//0b___11011
			case 0x13: gSensorIRData = -1; break;		//0b___10011
			case 0x17: gSensorIRData = -2; break;		//0b___10111
			case 0x07: gSensorIRData = -3; break;		//0b___00111
			case 0x0F: gSensorIRData = -4; break;		//0b___01111
			case 0x00:
			case 0x01:
			case 0x10:
				gSensorIRData = -10; break;
			default: break;
			#endif
    }
//   LOG_INFO("gSensorIRData: %d\n", gSensorIRData);
}
