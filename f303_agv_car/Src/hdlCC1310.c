/*
 * hdlCC1310.c
 *
 *  Created on: Mar 3, 2020
 *      Author: chungnguyen
 */
#include "uart_user.h"
#include "hdlCC1310.h"
#include <stdlib.h>
#include <string.h>

uint8_t coHang = 0;
uint8_t AGVrunning = 0;
/* TX CMD variables */
static char bufTx[50] = {0};
static uint8_t leng; 

typedef enum 
{
    AGV_STM_READY = 1,
    AGV_STM_RUNNING,
    AGV_STM_WAITING,
    AGV_STM_ERROR,
    AGV_STM_RETURN,
    AGV_STM_CHARGING,
    AGV_STM_HAS_WORKING,
    AGV_STM_NUM_STATION,
    AGV_STM_TIME_WORKING,
    AGV_STM_ERROR_CODE,
    AGV_STM_MAX_TX_CMD
} TXcmd_t;

static struct cmdTXStruct_t
{
    const uint8_t cmdId;
    const char nameCmd[25];
    uint16_t value;
} TxcmdStruct[AGV_STM_MAX_TX_CMD] = 
{
    /* cac gia tri duoi value co dinh */
    {0, NULL,           0},
    {1, "READY",        1},
    {1, "RUNNING",      2},
    {1, "WAITING",      3},
    {1, "ERROR",        4},
    {1, "RETURN",       5},
    {1, "CHARGING",     6},
    {7, "WORKING",      7},
    /* cac gia tri duoi thay doi duoc value */
    {8, "NUM_STATION",  0},
    {9, "TIME_WORKING", 0},
    {10, "ERROR_CODE",  0},
};
/* END TX CMD variables */
/* RX CMD variables */
typedef enum 
{
    AGV_CC_READY = 1,
    AGV_CC_EXE,
    AGV_CC_STATION_NUM,
    AGV_CC_MAX_RX_CMD
} RXcmd_t;

static struct cmdRXStruct_t
{
    const uint8_t cmdId;
    const char nameCmd[25];
    uint16_t value;
} RxcmdStruct[AGV_CC_MAX_RX_CMD] = 
{
    {0, NULL,           0},
    /* cac gia tri duoi value co dinh */
    {1, "READY",        1},
    {2, "EXE",          1},  
    /* cac gia tri duoi thay doi duoc value */
    {3, "STATION_NUM",  0},
};
/* END RX CMD variables */

typedef struct 
{
    uint8_t station;
    uint8_t dir;
} stationType;

stationType oldStation = {0, 0}, newStation = {0, 0};

#include "main.h"
void gotoNewStation(void)
{
		if(newStation.station == 0)
		{
			if(oldStation.dir == 1) { AGV_Re_Trai(); return; }
			if(oldStation.dir == 2) { AGV_Re_Phai(); return; }
		}
    if(oldStation.station == 0) AGV_Di_Thang();
		else AGV_Quay_Dau();
}

uint8_t checkDirSta(char *hdl, uint8_t station, uint8_t dir)
{
    if(station == newStation.station)
    {
			if(strcmp((const char*)hdl, "STA") == 0) 
			{
					oldStation.station = newStation.station;
					oldStation.dir = newStation.dir;
				
					if(station == 0) { HAL_UART_Transmit(uartStruct[uartCC1310].uartHdl, (uint8_t*)"$01,3#", 6, 1000); // tram 0 thi chuyen sang waiting
							HAL_Delay(10);
							AGVrunning = 0;
					}
					return DIR_DUNG;
			}
			else // chieu di: gap cross
			{
				if(newStation.station == oldStation.station) return DIR_DI_THANG;
				if(newStation.dir == 1) return DIR_RE_TRAI;
				if(newStation.dir == 2) return DIR_RE_PHAI;
			}
    }
		if(station == oldStation.station)
		{
			if(oldStation.dir == 1) return DIR_RE_TRAI;
			if(oldStation.dir == 2) return DIR_RE_PHAI;
		}
    return DIR_DI_THANG;
}

void AGVSTMpowerOn(void)
{
    HAL_Delay(1000);
    
}

void UART_handleCC1310(uint8_t *dataIn, uint8_t len)
{
    uint8_t i, c, strNum = 0;
    char *ptr[4];
    LOG_INFO("RX: %s\n", dataIn);
    if(dataIn[0] == '$' && dataIn[len-1] == '#')
    {
        // xu ly ban tin dung dinh dang o day
        /* Tach dau , lay truong du lieu $%d_%s_%d_%d# */
        ptr[strNum] = (char*)&dataIn[1];
        for(i = 1; i < len; i++)
        {
            c = dataIn[i];
            if(c == '_' || c == '#') 
            {
                dataIn[i] = '\0';
                if(c == '#') break;
                strNum++;
                ptr[strNum] = (char*)&dataIn[i+1];
            }
        }
				
				uint8_t cmd = atoi(ptr[0]);
				if(cmd == 1)
				{
//					HAL_UART_Transmit(uartStruct[uartCC1310].uartHdl, (uint8_t*)"$01,1#", 6, 1000);					
					if(coHang == 0) HAL_UART_Transmit(uartStruct[uartCC1310].uartHdl, (uint8_t*)"$07,0#", 6, 1000);					
					else HAL_UART_Transmit(uartStruct[uartCC1310].uartHdl, (uint8_t*)"$07,1#", 6, 1000);
					HAL_Delay(10);
					HAL_UART_Transmit(uartStruct[uartCC1310].uartHdl, (uint8_t*)"$08,0#", 6, 1000); // tram vua di qua
					HAL_Delay(10);
					HAL_UART_Transmit(uartStruct[uartCC1310].uartHdl, (uint8_t*)"$01,3#", 6, 1000); // waiting
					AGVrunning = 0;
					HAL_Delay(10);
				}
        else if(cmd == 3)
        {					
					if(strNum == 3)
					{
            // $STA_x_y#
            /* handle */
            char *hdl = ptr[1];
            newStation.station = atoi(ptr[2]);
            newStation.dir = atoi(ptr[3]);
            // if(c == AGV_CC_READY && v == RxcmdStruct[AGV_CC_READY].value)
            // {
            //     LOG_INFO("AGV_CC_READY\n");
								
            // }
            // else if(c == AGV_CC_STATION_NUM)
            // {
            //     RxcmdStruct[AGV_CC_STATION_NUM].value = v;
            //     AGV_STM_TX_CMD(3, 1);
            // }
            gotoNewStation();
						
						HAL_UART_Transmit(uartStruct[uartCC1310].uartHdl, (uint8_t*)"$03,1#", 6, 1000); // ack
						HAL_Delay(10);
						HAL_UART_Transmit(uartStruct[uartCC1310].uartHdl, (uint8_t*)"$01,2#", 6, 1000); // running
						AGVrunning = 1;
					}
        }
    }
    else LOG_INFO("cmd false\n");
}

void CC1310_updateOldSta(uint8_t sta, uint8_t dir)
{
	leng = sprintf(bufTx, "$08,%d%d#", sta, dir);
	HAL_UART_Transmit(uartStruct[uartCC1310].uartHdl, (uint8_t*)bufTx, leng, 1000); // tram vua di qua
	HAL_Delay(10);
}

void CC1310_updatePayload(uint8_t en)
{
	if(AGVrunning == 1)
	{
		if(en == 1) HAL_UART_Transmit(uartStruct[uartCC1310].uartHdl, (uint8_t*)"$07,1#", 6, 1000);
		else if(en == 0) 
		{
			HAL_UART_Transmit(uartStruct[uartCC1310].uartHdl, (uint8_t*)"$07,0#", 6, 1000);
			HAL_Delay(10);
			HAL_UART_Transmit(uartStruct[uartCC1310].uartHdl, (uint8_t*)"$01,3#", 6, 1000); // waiting
			AGVrunning = 0;
		}
	}
}

/* For check payload */
#define PAYLOAD_TIME_DEBOUNCE 20 // 20ms
#define PAYLOAD_ACTIVE_LEVEL 0
#define INVERT_ACTIVE_LEVEL (1-PAYLOAD_ACTIVE_LEVEL) 
enum
{
	PAYLOAD_NCHANGE = 0,
	PAYLOAD_DEBOUNCE,
} payloadStatus;
static uint8_t TemValue, curValue = INVERT_ACTIVE_LEVEL;
static uint32_t timeStartActive, timeTotalActive;
static uint8_t state = PAYLOAD_NCHANGE;
void CC1310_checkHasPayloadEvent(void)
{
	TemValue = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0);
	timeTotalActive = elapsedtime(g_SysTime, timeStartActive);
	switch(state)
	{
		case PAYLOAD_NCHANGE:
				if(TemValue != curValue)
				{
					timeStartActive = g_SysTime;
					state = PAYLOAD_DEBOUNCE;
				}
				break;
		case PAYLOAD_DEBOUNCE:
				if(timeTotalActive > PAYLOAD_TIME_DEBOUNCE)
				{
					if(TemValue != curValue)
					{
							curValue = TemValue;
						#if(PAYLOAD_ACTIVE_LEVEL == 0)
							CC1310_updatePayload(!curValue);
						#else
							CC1310_updatePayload(curValue);
						#endif
					}
					state = PAYLOAD_NCHANGE;
				} 
//				else if(TemValue == curValue)state = PAYLOAD_NCHANGE;
				break;
		}
}
