/*
 * uart_user.c
 *
 *  Created on: Jan 30, 2019
 *      Author: chungnguyen
 */
#include "uart_user.h"
//#define TEST_MES
uartStruct_t uartStruct[NUM_MODULE_UART] = 
{
	#ifdef TEST_MES
	{&huart1, uartCC1310, NULL},
	{&huart2, uartDEBUG,  UART_handleCC1310},
	#else
	{&huart1, uartCC1310, UART_handleCC1310},
	{&huart2, uartDEBUG,  NULL},
	#endif
	{&huart3, uartQR,	    UART_handleQR},	
};

static buffer_t bufferRx[NUM_MODULE_UART];

/************************************************************************************/
#ifdef __GNUC__
  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE  
{
    /* Place your implementation of fputc here */
    /* e.g. write a character to the USART */
    HAL_UART_Transmit(uartStruct[uartDEBUG].uartHdl, (uint8_t *)&ch, 1, 100);
    return ch;
}
/************************************************************************************/
static void getNewByte(uartType_t i)
{
	bufferRx[i].Data[bufferRx[i].Index] = bufferRx[i].Byte;
	bufferRx[i].Index++;
	bufferRx[i].State = 3;
	HAL_UART_Receive_IT(uartStruct[i].uartHdl, &bufferRx[i].Byte, 1);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	for(uartType_t i = uartCC1310; i < NUM_MODULE_UART; i++)
	{
		if(huart->Instance == uartStruct[i].uartHdl->Instance)
		{
			getNewByte(i);
		}
	}
}

void Uart_User_Init(void)
{
	for(uartType_t i = uartCC1310; i < NUM_MODULE_UART; i++ )
		HAL_UART_Receive_IT(uartStruct[i].uartHdl, &bufferRx[i].Byte, 1);
	LOG_INFO("Init UART done!\r\n");
}

void Uart_User_Process(void)
{
	for(uartType_t i = uartCC1310; i < NUM_MODULE_UART; i++ )
	{
		if(bufferRx[i].State > 0)
		{
			bufferRx[i].State--;
			if(bufferRx[i].State == 0)
			{
				if(uartStruct[i].func != NULL)
				{
					bufferRx[i].Data[bufferRx[i].Index] = 0;
					uartStruct[i].func(bufferRx[i].Data, bufferRx[i].Index);
				}
				bufferRx[i].Index = 0;
			}
		}
	}
}
