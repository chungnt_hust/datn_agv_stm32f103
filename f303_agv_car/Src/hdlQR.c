/*
 * hdlQR.c
 *
 *  Created on: Mar 9, 2020
 *      Author: chungnguyen
 */
#include "uart_user.h"
#include "hdlQR.h"
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "hdlCC1310.h"

typedef struct
{
	char hdl[5];
	uint8_t sta;
	uint8_t dir;
} QRtype;

QRtype newQR, oldQR;

void UART_handleQR(uint8_t *dataIn, uint8_t len)
{
		uint8_t i, c, strNum = 0;
    char *ptr[3];
    LOG_INFO("QR RX: %s\n", dataIn);
    if(dataIn[len-1] == '\n')
    {
				dataIn[len-2] = '\n';
        // xu ly ban tin dung dinh dang o day
        /* Tach dau _ lay truong du lieu %s_%d_%d\r\n */
        ptr[strNum] = (char*)&dataIn[0];
        for(i = 1; i < len; i++)
        {
            c = dataIn[i];
            if(c == '_' || c == '\n') 
            {
                dataIn[i] = '\0';
                if(c == '\n') break;
                strNum++;
                ptr[strNum] = (char*)&dataIn[i+1];
            }
        }
        if(strNum > 0)
        {					
            // [STA_x_y] [CR_x_y]
            /* handle */
						// char *hdl = ptr[0];
            strcpy(newQR.hdl, ptr[0]);
            newQR.sta = atoi(ptr[1]);
            newQR.dir = atoi(ptr[2]);
						CC1310_updateOldSta(newQR.sta, newQR.dir);
//						if(newQR.sta != oldQR.sta && newQR.dir != oldQR.dir)
//						{
							switch(checkDirSta(newQR.hdl, newQR.sta, newQR.dir))
							{
								case DIR_DUNG: AGV_Dung_lai(); break;
								case DIR_RE_TRAI: AGV_Re_Trai(); break;
								case DIR_RE_PHAI: AGV_Re_Phai(); break;
							}
//						}
						oldQR = newQR;
							
						
        }
    }
    else LOG_INFO("QR cmd false\n");
}
