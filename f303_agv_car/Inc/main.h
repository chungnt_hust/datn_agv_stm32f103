/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2020 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */
#include "stm32f3xx_hal.h"
#include <stdint.h>

#define DEBUG_EN 1

#if DEBUG_EN == 1
#define LOG_INFO(string, ...) printf(string, ##__VA_ARGS__)  
#else
#define LOG_INFO(string, ...)
#endif
/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/
#define COMMUNICATE_CC1310_BAUDRATE 115200
#define DEBUG_BAUDRATE 115200
#define SCANNER_QR_BAUDRATE 115200

#define led_Pin GPIO_PIN_13
#define led_GPIO_Port GPIOC
#define PAYLOAD_SENSOR_Pin GPIO_PIN_0
#define PAYLOAD_SENSOR_GPIO_Port GPIOA
#define IR1_Pin GPIO_PIN_4
#define IR1_GPIO_Port GPIOA
#define IR2_Pin GPIO_PIN_5
#define IR2_GPIO_Port GPIOA
#define IR3_Pin GPIO_PIN_6
#define IR3_GPIO_Port GPIOA
#define IR4_Pin GPIO_PIN_7
#define IR4_GPIO_Port GPIOA
#define IR5_Pin GPIO_PIN_0
#define IR5_GPIO_Port GPIOB
#define LED_R_Pin GPIO_PIN_14
#define LED_R_GPIO_Port GPIOB
#define LED_G_Pin GPIO_PIN_15
#define LED_G_GPIO_Port GPIOB
#define LED_B_Pin GPIO_PIN_8
#define LED_B_GPIO_Port GPIOA
#define DIR_CH1_Pin GPIO_PIN_4
#define DIR_CH1_GPIO_Port GPIOB
#define DIR_CH2_Pin GPIO_PIN_5
#define DIR_CH2_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */
									/* Huong di chuyen cua xe */
/*===========================================================================*/
enum
{
	DIR_DUNG = 0,
	DIR_DI_THANG,
	DIR_RE_TRAI,
	DIR_RE_PHAI,
	DIR_QUAY_DAU,
};
/*===========================================================================*/
extern volatile uint32_t g_SysTime;
uint32_t elapsedtime(uint32_t newTime, uint32_t oldTime);
void AGV_Dung_lai(void);
void AGV_Di_Thang(void);
void AGV_Re_Trai(void);
void AGV_Re_Phai(void);
void AGV_Quay_Dau(void);
/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
