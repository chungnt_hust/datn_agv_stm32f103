/*
 * hdlCC1310.h
 *
 *  Created on: Mar 3, 2020
 *      Author: chungnguyen
 */

#ifndef HDLCC1310_H_
#define HDLCC1310_H_

#if defined(STM32F1)
#include "stm32f1xx_hal.h"
#elif defined(STM32F2)
#include "stm32f2xx_hal.h"
#elif defined(STM32F3)
#include "stm32f3xx_hal.h"
#elif defined(STM32F4)
#include "stm32f4xx_hal.h"
#endif
uint8_t checkDirSta(char *hdl, uint8_t station, uint8_t dir);
void AGVSTMpowerOn(void);
void CC1310_updateOldSta(uint8_t sta, uint8_t dir);
void CC1310_updatePayload(uint8_t en);
void CC1310_checkHasPayloadEvent(void);
#endif /* HDLCC1310_H_ */
