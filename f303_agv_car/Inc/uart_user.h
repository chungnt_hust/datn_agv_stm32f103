/*
 * uart_user.h
 *
 *  Created on: Jan 30, 2019
 *      Author: chungnguyen
 */

#ifndef UART_USER_H_
#define UART_USER_H_

#if defined(STM32F1)
#include "stm32f1xx_hal.h"
#elif defined(STM32F2)
#include "stm32f2xx_hal.h"
#elif defined(STM32F3)
#include "stm32f3xx_hal.h"
#elif defined(STM32F4)
#include "stm32f4xx_hal.h"
#endif

#include "usart.h"
#include <stdio.h>

#define MAX_NUM_DATA 100
typedef struct
{
	uint8_t Data[MAX_NUM_DATA];
	uint8_t Byte;
	uint8_t Index;
	uint8_t State;
} buffer_t;

typedef enum 
{
	uartCC1310 = 0,
	uartDEBUG,  
	uartQR,
	NUM_MODULE_UART
} uartType_t;

typedef struct 
{
	UART_HandleTypeDef *uartHdl;
	uartType_t uartType;
	void (*func)(uint8_t *, uint8_t);
} uartStruct_t;

extern uartStruct_t uartStruct[NUM_MODULE_UART];

void Uart_User_Init(void);
void Uart_User_Process(void);
void UART_handleCC1310(uint8_t *dataIn, uint8_t len);
void UART_handleQR(uint8_t *dataIn, uint8_t len);
#endif /* UART_USER_H_ */
