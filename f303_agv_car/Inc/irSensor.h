/*
 * irSensor.h
 *
 *  Created on: Mar 11, 2020
 *      Author: chungnguyen
 */
#ifndef IRSENSOR_H_
#define IRSENSOR_H_
#if defined(STM32F1)
#include "stm32f1xx_hal.h"
#elif defined(STM32F2)
#include "stm32f2xx_hal.h"
#elif defined(STM32F3)
#include "stm32f3xx_hal.h"
#elif defined(STM32F4)
#include "stm32f4xx_hal.h"
#endif

#define IR_TIME_DEBOUNCE 20

typedef enum
{
	IR_STATE_NCHANGE = 0,
	IR_STATE_DEBOUNCE,
	IR_STATE_CHANGE,
} IR_State_t;

typedef enum
{
	IR_1 = 0,
	IR_2,
	IR_3,
    IR_4,
    IR_5,
	IR_MAX_PIN
} IR_Pin_t;

extern int8_t gSensorIRData;
void IR_Scan(void);
#endif /* IRSENSOR_H_ */
